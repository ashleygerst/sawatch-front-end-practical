import {
  Route,
  Switch,
  NavLink,
  BrowserRouter as Router,
} from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import PracticalMap from './components/practicalMap';
import PracticalGraph from './components/practicalGraph';
import PracticalTable from './components/practicalTable';
import './App.css';
import 'leaflet/dist/leaflet.css';

/*
                   *** GLOSSARY ***

  This is for applicants and not something we'd generally
  put into production code.

  GHG - Green House Gas emissions
  NOX - A group of Nitrogen Oxide compounds (included in GHG).
  CO - Carbon Monoxide, not a GHG, but not great.
  PM 10 - Particulate matter sized around 10 micrometers.
          A human hair is 70-100 micrometers in width.
          This dusty stuff gets stuck in the nose and upper respitory system.
  PM 2.5 - Particulate matter sized around 2.5 micrometers.
           It's more likely to get deep inside the lungs.
           Also not great.
  Emission Ratio - This is a bespoke measurment of emissions / miles
          Vehicles are meant to be driven. We don't want to ping them
          for doing their job, but we do want to differentiate cleaner
          miles from dirtier mile, which is why wer surface this ratio.
  Fuel Economy - Estimate miles per gallon.
*/

const API_URL = 'https://dev-api.sawatchlabs.com/';
const EMISSIONS_URL = `${API_URL}/emissionsForPractical`;
const MILAGE_URL = `${API_URL}/milageDataforPractical`;
const PARKING_URL = `${API_URL}/parkingEventsForPractical`;

const tableColumns = [
  {
    Header: 'Asset Id',
    accessor: 'assetId',
    sortType: 'basic',
    Cell: (props) => {
      const vin = props.row.original.vin.slice(0, 3);
      return !props.value ? vin : props.value;
    },
  },
  { Header: 'Class', accessor: 'vehicleClass', sortType: 'basic' },
  { Header: 'Miles', accessor: 'miles', sortType: 'basic' },
  { Header: 'Emission Ratio', accessor: 'emit', sortType: 'basic' },
  { Header: 'Fuel Economy', accessor: 'fuelEcon', sortType: 'basic' },
  { Header: 'GHG tons', accessor: 'ghg', sortType: 'basic' },
  { Header: 'NOX lbs', accessor: 'nox', sortType: 'basic' },
  { Header: 'CO lbs', accessor: 'co', sortType: 'basic' },
  { Header: 'PM 10 lbs', accessor: 'pm10', sortType: 'basic' },
  { Header: 'PM 2.5 lbs', accessor: 'pm25', sortType: 'basic' },
  { Header: 'Year', accessor: 'year', sortType: 'basic' },
  {
    Header: 'Make',
    accessor: 'make',
    sortType: 'basic',
    Cell: (props) => {
      const lowerCase = props.value
        .toLowerCase()
        .substring(1, props.value.length);
      const capitalized = lowerCase.replace(/(-)(\w)/g, (match) => {
        return match.toUpperCase();
      });
      return props.value.charAt(0) + capitalized;
    },
  },
  { Header: 'Model', accessor: 'model', sortType: 'basic' },
];

function App() {
  // pre-defined shape
  const [emissions, setEmissions] = useState([]);
  const [milage, setMilage] = useState([]);
  const [parking, setParking] = useState([]);

  useEffect(() => {
    fetch(EMISSIONS_URL, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error('Bad Request');
        }
      })
      .then((data) => setEmissions(data.data));
    fetch(MILAGE_URL, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error('Bad Request');
        }
      })
      .then((data) => setMilage(data.data));
    fetch(PARKING_URL, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((res) => {
        if (res.ok) {
          return res.json();
        } else {
          throw new Error('Bad Request');
        }
      })
      .then((data) => setParking(data.data));
  }, []);

  const PracticalLink = ({ id, label }) => {
    return (
      <NavLink
        style={{
          border: 'solid',
          borderColor: '#126185',
          color: '#126185',
          fontWeight: 'bold',
          textDecoration: 'none',
          height: 30,
        }}
        activeStyle={{
          color: 'white',
          fontWeight: 'bold',
          backgroundColor: '#126185',
        }}
        to={`/${id}`}
        id={id}
      >
        {label}
      </NavLink>
    );
  };
  document.body.style = 'background: #ebebeb;';

  return (
    <Router>
      <div className='app-wrapper'>
        <nav className='app-button-wrapper'>
          <PracticalLink id={'table'} label={'Table'} />
          <PracticalLink id={'graph'} label={'Graph'} />
          <PracticalLink id={'map'} label={'Map'} />
        </nav>
        <div className='app-tab-wrapper'>
          <Switch>
          <Route path='/table' render={() => (<PracticalTable columns={tableColumns} data={emissions} />)} />
            <Route exact path='/graph' render={() => (<PracticalGraph data={milage} />)} />
            <Route exact path='/map' render={() => (<PracticalMap data={parking} />)} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
