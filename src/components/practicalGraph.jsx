import React from 'react';
import { Line } from 'react-chartjs-2';
import '../styles/Graph.css';
import moment from 'moment';

export default function PracticalGraph(props) {
  const { data } = props;
  const labels = data.map((r) => {
    return `${moment(r.month).format('MMMM')} ${r.year}`;
  });
  const miles = data.map((r) => {
    return parseFloat(r.miles);
  });
  const trips = data.map((r) => {
    return parseInt(r.count);
  });
  const ghgLbs = data.map((r) => {
    return parseFloat(r.ghgLbs);
  });
  const dataSet = {
    type: 'line',
    labels:
      labels.length === data.length
        ? labels
        : new Array(data.length).fill('Data'),
    datasets: [
      {
        label: 'Miles',
        data: miles,
        backgroundColor: 'rgba(75,192,192,0.2)',
        borderColor: 'rgba(75,192,192,1)',
        fill: true,
      },
      {
        label: 'GHG',
        data: ghgLbs,
        fill: false,
        borderColor: '#742774',
      },
      {
        label: 'Trips',
        data: trips,
        fill: false,
        borderColor: '#A32468',
      },
    ],
  };
  const options = {
    layout: {
      justifyContent: 'center',
      padding: {
        top: 15,
      },
    },
    responsive: true,
    title: {
      display: true,
      text: 'Graph',
      fontSize: 25,
    },
    tooltips: {
      mode: 'index',
      intersect: false,
      hover: {
        mode: 'nearest',
        intersect: true,
      },
    },
    legend: {
      position: 'left',
    },
    maintainAspectRatio: true,
    scales: {
      yAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
          },
        },
      ],
      xAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Month',
            fontSize: 20,
          },
        },
      ],
    },
  };

  return (
    <div className='graph'>
      <Line
        type='line'
        className='graph-container'
        id='canvas'
        data={dataSet}
        options={options}
      ></Line>
    </div>
  );
}
