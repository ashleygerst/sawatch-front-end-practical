import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import BTable from 'react-bootstrap/Table';
import '../styles/Table.css';
import { useTable, useSortBy } from 'react-table7';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons';

export default function PracticalTable(props) {
  const { columns, data } = props;
  const icons = {
    caretUp: <FontAwesomeIcon icon={faCaretUp} />,
    caretDown: <FontAwesomeIcon icon={faCaretDown} />,
  };
  const sort = 'sort';

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable(
    {
      columns,
      data,
    },
    useSortBy
  );

  return (
    <BTable
      sticky='top'
      className='tableContainer'
      hover
      size='sm'
      {...getTableProps()}
    >
      <thead sticky className='tableHeader'>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th
                className='headerRow'
                {...column.getHeaderProps(column.getSortByToggleProps())}
              >
                {column.render('Header')}
                <span className='sortContainer'>
                  {column.isSorted ? (
                    column.isSortedDesc ? (
                      icons.caretUp
                    ) : (
                      icons.caretDown
                    )
                  ) : (
                    <div className='sortButton'>
                      <div className='sortText'>{sort}</div>
                    </div>
                  )}
                </span>
              </th>
            ))}
          </tr>
        ))}
      </thead>

      <tbody className='rowStyle' {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => {
                return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>;
              })}
            </tr>
          );
        })}
      </tbody>
    </BTable>
  );
}
